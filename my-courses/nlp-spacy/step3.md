## Rule-based morphology

<br/>

#### É o processo pelo qual uma forma de raiz de uma palavra é modificada pela adição de prefixos ou sufixos que especificam sua função gramatical, mas não alteram sua classe gramatical.

<br/>

#### Dizemos que um lema (`root form`) é flexionado com uma ou mais características morfológicas para criar uma forma de superfície. aqui estão alguns exemplos:

| Contexto | Superfície    | Lemma  | PoS   | Features | 
|----------|:-------------:|-------:|------:|---------:|
| I was reading the paper | reading | $1600  | $1600 | $1600|
| I don’t watch the news, I read the paper |    read   | $12    | $12   |
| I read the paper yesterday | read |    $1  |

#### É aqui que entra o modelo estatístico, que permite ao `SpaCy` fazer uma previsão de qual tag ou rótulo provavelmente se aplica neste contexto.

##### Obs. Nesse contexto, `label`, `rótulo`, ou `tag` se referem a uma coisa só que seria resgatar a glasse gramatical daquela palavra dado seu contexto na frase

<br/>

#### Após copiar e colar no `Colab`, execute o bloco de código e ao final desse passo nós teremos algo parecido com essa imagem
