<br/>

# Bem vindo ao curso rápido de introdução ao SpaCy

#### Desde já gostaria de específicar que para cumprir as etapas desse cenário você tem duas opções

<br/>

1. Configurar sua máquina para estar disponível ao comando do SpaCy

    1.1. Para isso veja o quickstart aqui https://spacy.io/usage

2. Utilizar o Google Colab para encurtar o tempo

#### OBS. Vou estar utilizando o `Colab` durante todo o cenário. Se você não conhece essa ferramente incrível, pare tudo que está fazendo e dê uma olhada. Também tornarei o conteúdo mais propenso a utilização do Colab, caso você não a conheça, fique tranquilo, vou demonstrar passo a passo