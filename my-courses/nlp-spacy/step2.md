## Part-of-speech tagging 

<br/>

#### No `NLTK` é necessário que façamos a `tokenização` do texto para daí fazermos o processor de `PoS Tagging`, no `SpaCy` é tudo uma coisa só.

<br/>

#### Vou pedir agora que você vá ao [google colab](https://colab.research.google.com/notebooks/intro.ipynb#recent=true) e crie seu primeiro notebook e cole lá este trecho de código

```
import spacy

nlp = spacy.load("en_core_web_sm")

doc = nlp("Apple is looking at buying U.K. startup for $1 billion")

for token in doc:
  print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_, token.shape_, token.is_alpha, token.is_stop)

```

#### É aqui que entra o modelo estatístico, que permite ao `SpaCy` fazer uma previsão de qual tag ou rótulo provavelmente se aplica neste contexto.

##### Obs. Nesse contexto, `label`, `rótulo`, ou `tag` se referem a uma coisa só que seria resgatar a glasse gramatical daquela palavra dado seu contexto na frase

<br/>

#### Após copiar e colar no `Colab`, execute o bloco de código e ao final desse passo nós teremos algo parecido com essa imagem
