<br/>

# Linguistic Features

<br/>

#### Processar texto bruto de forma inteligente é difícil: a maioria das palavras são raras e é comum que palavras que parecem completamente diferentes signifiquem quase a mesma coisa. O processo de tratamento de texto, desde seu início com a `tokenização`, `setence splitting`, `PoS Tagging`, etc. Pode ser muito trabalhoso, é exatamente por isso que existem ferramentas como o `NLTK` e o `SpaCy`